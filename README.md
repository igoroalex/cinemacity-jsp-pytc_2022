# CinemaCity
### Graduation project JSP PyTC_2022

CinemaCity provides opportunities to purchase tickets for a particular session, as well as search for available sessions by film title, genre, actors, director, date and time of screening. Also the ability to sort available sessions by date.

The viewer, in addition to finding an available session for watching a certain movie, has the opportunity to see which seats in the hall are available for a particular session, and buy tickets. And the viewer has the opportunity to view the history of ticket purchases. The viewer can also change their password.

The administrator has the ability to add a new movie and create a new session for a specific movie in a specific room. It is worth noting that each hall has a different number of seats. It is also possible to view the statistics of the occupancy of the halls, the number of tickets purchased for a particular movie.

# How to run via docker
1. docker-compose up


# How to run it
1. pip install -r requirements.txt
2. From root: ```python3 run.py```
