#
FROM python:3.10-alpine

#
WORKDIR /cinema_city

#
COPY ./requirements.txt /cinema_city/requirements.txt

#
RUN pip install -r /cinema_city/requirements.txt

#
COPY ./app /cinema_city/app
COPY ./run.py /cinema_city/run.py
COPY ./README.md /cinema_city/README.md
COPY ./config.py /cinema_city/config.py
COPY ./.env.prod /cinema_city/.env
