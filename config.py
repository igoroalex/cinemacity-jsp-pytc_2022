"""
Init settings with common parameters
"""
from pydantic import BaseSettings


class Settings(BaseSettings):
    """
    App's settings
    """
    APP_NAME: str = "CinemaCity"
    HOST: str
    PORT: int
    RELOAD: bool
    SQLALCHEMY_DATABASE_URL: str
    CHECK_SAME_THREAD: bool
    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int
    REFRESH_TOKEN_EXPIRE_DAYS: int

    class Config:
        env_file = ".env"


settings = Settings()
