"""
Tests routers of movies
"""


def test_create_movie(client):
    body = {"imdb_id": "01330934", "title": "Matrix", "with_imdb": False}
    resp = client.post("/api/movies/", json=body)
    assert resp.status_code == 201


def test_create_same_movie(client):
    body = {"imdb_id": "01330934", "title": "Matrix", "with_imdb": False}
    resp = client.post("/api/movies/", json=body)
    assert resp.status_code == 400


def test_get_movie(client):
    resp = client.get("/api/movies/1")
    assert resp.status_code == 200


def test_get_none_movie(client):
    resp = client.get("/api/movies/100")
    assert resp.status_code == 404


def test_get_movies(client):
    resp = client.get("/api/movies")
    assert resp.status_code == 200


def test_patch_movie(client):
    body = {"title": "Matrix 2"}
    resp = client.patch("/api/movies/1", json=body)
    assert resp.status_code == 200


def test_patch_none_movie(client):
    body = {"title": "Matrix"}
    resp = client.patch("/api/movies/100", json=body)
    assert resp.status_code == 404


def test_delete_movie(client):
    resp = client.delete("/api/movies/1")
    assert resp.status_code == 200


def test_delete_none_movie(client):
    resp = client.delete("/api/movies/100")
    assert resp.status_code == 404
