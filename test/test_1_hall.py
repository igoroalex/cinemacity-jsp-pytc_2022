"""
Tests routers of halls
"""


def test_create_hall(client):
    body = {"name": "gray", "number_of_seats": 52}
    resp = client.post("/api/halls/", json=body)
    assert resp.status_code == 201


def test_create_same_hall(client):
    body = {"name": "gray", "number_of_seats": 52}
    resp = client.post("/api/halls/", json=body)
    assert resp.status_code == 404


def test_get_hall(client):
    resp = client.get("/api/halls/1")
    assert resp.status_code == 200


def test_get_none_hall(client):
    resp = client.get("/api/halls/100")
    assert resp.status_code == 404


def test_get_halls(client):
    resp = client.get("/api/halls")
    assert resp.status_code == 200


def test_patch_hall(client):
    body = {"name": "green", "number_of_seats": 21}
    resp = client.patch("/api/halls/1", json=body)
    assert resp.status_code == 200


def test_patch_none_hall(client):
    body = {"name": "green", "number_of_seats": 21}
    resp = client.patch("/api/halls/100", json=body)
    assert resp.status_code == 404


def test_delete_hall(client):
    resp = client.delete("/api/halls/1")
    assert resp.status_code == 200


def test_delete_none_hall(client):
    resp = client.delete("/api/halls/100")
    assert resp.status_code == 404
