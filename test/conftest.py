import pytest
from fastapi.testclient import TestClient

from app.api.dependencies import get_current_active_user
from app.database import engine, Base
from app.main import app


@pytest.fixture
def client(skip_authentication):
    return TestClient(app)


@pytest.fixture
def skip_authentication() -> None:

    def get_current_user():
        pass
    app.dependency_overrides[get_current_active_user] = get_current_user


@pytest.fixture()
def test_db():
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    # yield
