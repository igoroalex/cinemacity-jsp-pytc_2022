"""
Tests routers of users
"""


def test_add_users(client):

    def add_user(body):
        resp = client.post("/api/users/", json=body)
        assert resp.status_code == 201

    add_user({
        "email": "newviewer@gmail.com",
        "name": "aaa",
        "password": "aaa",
        "role_id": 2,
    })


def test_create_role(client):
    body = {"name": "manager"}

    resp = client.post("/api/roles/", json=body)
    assert resp.status_code == 201


def test_create_user(client):
    body = {
        "email": "asd@gmail.com",
        "name": "asd",
        "password": "1234",
        "role_id": 1,
    }

    resp = client.post("/api/users/", json=body)
    assert resp.status_code == 201


def test_create_user_non_role(client):
    body = {
        "email": "asd1@gmail.com",
        "name": "asd",
        "password": "1234",
        "role_id": 4,
    }

    resp = client.post("/api/users/", json=body)
    assert resp.status_code == 201


def test_create_same_user(client):
    body = {
        "email": "asd@gmail.com",
        "name": "asd",
        "password": "1234",
        "role_id": 1,
    }

    resp = client.post("/api/users/", json=body)
    assert resp.status_code == 400


def test_get_user(client):
    resp = client.get("/api/users/1")
    assert resp.status_code == 200


def test_get_none_user(client):
    resp = client.get("/api/users/100")
    assert resp.status_code == 404


def test_get_users(client):
    resp = client.get("/api/users")
    assert resp.status_code == 200


def test_patch_user(client):
    body = {"name": "asd11", "password": "456"}

    resp = client.patch("/api/users/1", json=body)
    assert resp.status_code == 200


def test_patch_none_user(client):
    body = {"name": "asd11", "password": "456"}

    resp = client.patch("/api/users/100", json=body)
    assert resp.status_code == 404


def test_delete_user(client):
    resp = client.delete("/api/users/1")
    assert resp.status_code == 200


def test_delete_none_user(client):
    resp = client.delete("/api/users/100")
    assert resp.status_code == 404
