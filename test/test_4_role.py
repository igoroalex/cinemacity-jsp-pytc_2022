"""
Tests routers of roles
"""


def test_create_role(client):
    body = {"name": "CEO"}

    resp = client.post("/api/roles/", json=body)
    assert resp.status_code == 201


def test_create_same_role(client):
    body = {"name": "CEO"}
    resp = client.post("/api/roles/", json=body)
    assert resp.status_code == 400


def test_get_role(client):
    resp = client.get("/api/roles/1")
    assert resp.status_code == 200


def test_get_none_role(client):
    resp = client.get("/api/roles/100")
    assert resp.status_code == 404


def test_get_roles(client):
    resp = client.get("/api/roles")
    assert resp.status_code == 200


def test_patch_role(client):
    body = {"name": "СШЩ"}
    resp = client.patch("/api/roles/1", json=body)
    assert resp.status_code == 200


def test_patch_none_role(client):
    body = {"name": "asd11"}

    resp = client.patch("/api/roles/100", json=body)
    assert resp.status_code == 404


def test_delete_role(client):
    resp = client.delete("/api/roles/1")
    assert resp.status_code == 200


def test_delete_none_role(client):
    resp = client.delete("/api/roles/100")
    assert resp.status_code == 404
