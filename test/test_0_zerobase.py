"""
Tests routers of users
"""


def test_init_db(test_db):
    pass


def test_add_movies(client):
    def add_movie(request_body):
        resp = client.post("/api/movies/", json=request_body)
        assert resp.status_code == 201

    add_movie({
        "imdb_id": "0133093",
        "title": "Matrix",
        "with_imdb": False,
    })
    add_movie({
        "imdb_id": "1375666",
        "title": "Inception",
        "with_imdb": False,
    })


def test_add_halls(client):

    def add_hall(request_body):
        resp = client.post("/api/halls/", json=request_body)
        assert resp.status_code == 201

    add_hall({
        "name": "green",
        "number_of_seats": 21,
    })
    add_hall({
        "name": "blue",
        "number_of_seats": 10,
    })


def test_add_sessions_movies(client):

    def add_session_movie(request_body):
        resp = client.post("/api/sessions_movie/", json=request_body)
        assert resp.status_code == 201

    add_session_movie({
        "date": "2022-05-18",
        "time": "21:50",
        "hall_id": 1,
        "movie_id": 1,
    })
    add_session_movie({
        "date": "2022-05-20",
        "time": "22:00",
        "hall_id": 2,
        "movie_id": 1,
    })
    add_session_movie({
        "date": "2022-05-30",
        "time": "22:50",
        "hall_id": 1,
        "movie_id": 2,
    })


def test_add_roles(client):

    def add_role(request_body):
        resp = client.post("/api/roles/", json=request_body)
        assert resp.status_code == 201

    add_role({
        "name": "admin",
    })
    add_role({
        "name": "viewer",
    })


def test_add_users(client):

    def add_user(request_body):
        resp = client.post("/api/users/", json=request_body)
        assert resp.status_code == 201

    add_user({
        "email": "admin@gmail.com",
        "name": "aaa",
        "password": "aaa",
        "role_id": 1,
    })
    add_user({
        "email": "viewer@gmail.com",
        "name": "aaa",
        "password": "aaa",
        "role_id": 2,
    })


def test_add_tickets(client):

    def add_ticket(request_body):
        resp = client.post("/api/tickets/", json=request_body)
        assert resp.status_code == 201

    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 1})
    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 11})
    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 12})
    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 13})
    add_ticket({"user_id": 1, "session_movie_id": 3, "seat": 15})
    add_ticket({"user_id": 1, "session_movie_id": 3, "seat": 13})
