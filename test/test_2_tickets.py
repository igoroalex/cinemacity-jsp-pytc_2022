"""
Tests routers of tickets
"""


def test_create_ticket(client):

    body = {"user_id": 1, "session_movie_id": 1, "seat": 2}
    resp = client.post("/api/tickets/", json=body)
    assert resp.status_code == 201


def test_create_same_ticket(client):
    body = {"user_id": 1, "session_movie_id": 1, "seat": 2}
    resp = client.post("/api/tickets/", json=body)
    assert resp.status_code == 409


def test_create_ticket_none_user(client):
    body = {"user_id": 100, "session_movie_id": 1, "seat": 3}
    resp = client.post("/api/tickets/", json=body)
    assert resp.status_code == 404


def test_create_ticket_none_session(client):
    body = {"user_id": 1, "session_movie_id": 100, "seat": 3}
    resp = client.post("/api/tickets/", json=body)
    assert resp.status_code == 404


def test_get_ticket(client):
    resp = client.get("/api/tickets/1")
    assert resp.status_code == 200


def test_get_none_ticket(client):
    resp = client.get("/api/tickets/100")
    assert resp.status_code == 404


def test_get_tickets(client):
    resp = client.get("/api/tickets")
    assert resp.status_code == 200


def test_patch_ticket(client):
    body = {"user_id": 1, "session_movie_id": 2, "seat": 2}

    resp = client.patch("/api/tickets/1", json=body)
    assert resp.status_code == 200


def test_patch_none_ticket(client):
    body = {"user_id": 1, "session_movie_id": 1, "seat": 2}

    resp = client.patch("/api/tickets/100", json=body)
    assert resp.status_code == 404


def test_delete_ticket(client):
    resp = client.delete("/api/tickets/1")
    assert resp.status_code == 200


def test_delete_none_ticket(client):
    resp = client.delete("/api/tickets/100")
    assert resp.status_code == 404
