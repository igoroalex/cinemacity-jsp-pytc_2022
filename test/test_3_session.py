"""
Tests routers of sessions_movies
"""


def test_create_sessions_movie(client):

    body = {"date": "2022-05-18", "time": "21:50", "hall_id": 2, "movie_id": 1}
    resp = client.post("/api/sessions_movie/", json=body)
    assert resp.status_code == 201


def test_create_same_sessions_movie(client):
    body = {"date": "2022-05-18", "time": "21:50", "hall_id": 2, "movie_id": 1}
    resp = client.post("/api/sessions_movie/", json=body)
    assert resp.status_code == 404


def test_create_sessions_movie_none_hall(client):
    body = {"date": "2022-05-18", "time": "21:50", "hall_id": 100, "movie_id": 1}
    resp = client.post("/api/sessions_movie/", json=body)
    assert resp.status_code == 404


def test_create_sessions_movie_none_movie(client):
    body = {"date": "2022-05-18", "time": "21:50", "hall_id": 2, "movie_id": 100}
    resp = client.post("/api/sessions_movie/", json=body)
    assert resp.status_code == 404


def test_create_sessions_movie_none_date(client):
    body = {"time": "21:50", "hall_id": 2, "movie_id": 100}
    resp = client.post("/api/sessions_movie/", json=body)
    assert resp.status_code == 422


def test_create_sessions_movie_none_time(client):
    body = {"date": "2022-05-18", "hall_id": 2, "movie_id": 100}
    resp = client.post("/api/sessions_movie/", json=body)
    assert resp.status_code == 422


def test_get_sessions_movie(client):
    resp = client.get("/api/sessions_movie/1")
    assert resp.status_code == 200


def test_get_none_sessions_movie(client):
    resp = client.get("/api/sessions_movie/100")
    assert resp.status_code == 404


def test_get_sessions_movies(client):
    resp = client.get("/api/sessions_movie")
    assert resp.status_code == 200


def test_patch_sessions_movie(client):
    body = {"date": "2022-05-18", "time": "21:30", "hall_id": 2, "movie_id": 2}
    resp = client.patch("/api/sessions_movie/1", json=body)
    assert resp.status_code == 200


def test_patch_none_sessions_movie(client):
    body = {"date": "2022-05-18", "time": "21:40", "hall_id": 2, "movie_id": 1}
    resp = client.patch("/api/sessions_movie/100", json=body)
    assert resp.status_code == 404


def test_delete_sessions_movie(client):
    resp = client.delete("/api/sessions_movie/1")
    assert resp.status_code == 200


def test_delete_none_sessions_movie(client):
    resp = client.delete("/api/sessions_movie/100")
    assert resp.status_code == 404
