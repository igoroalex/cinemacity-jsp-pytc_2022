from fastapi import Security, Depends, Form, HTTPException, APIRouter
from sqlalchemy.orm import Session
from starlette import status
from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse

from app import models as models, schemas as schemas
from app.database import get_db
from app.front_end.dependencies import get_current_user, templates


router = APIRouter(tags=["front-end"])


@router.get("/create_session/{movie_id}", response_class=HTMLResponse)
async def create_session_html(request: Request,
                              movie_id: str,
                              cur_user: models.User = Security(get_current_user, scopes=["admin"]),
                              db: Session = Depends(get_db)):
    """
    Create session movie
    """
    db_movie = models.Movie.get_first(db, filters={"id": movie_id})
    response = schemas.MovieCard.from_orm(db_movie)
    return templates.TemplateResponse("session_create.html", {"request": request, "response": response})


@router.post("/create_session_movie", response_class=HTMLResponse)
async def create_session_movie_html(request: Request,
                                    movie_id: str = Form(...),
                                    session_date: str = Form(...),
                                    session_time: str = Form(...),
                                    hall_id: str = Form(...),
                                    cur_user: models.User = Security(get_current_user, scopes=["admin"]),
                                    db: Session = Depends(get_db)):
    """
    Create session movie
    """
    request_body = {
        "date": session_date,
        "time": session_time,
        "hall_id": hall_id,
        "movie_id": movie_id,
    }
    cur_model = schemas.SessionMovieCreate(**request_body)
    db_session_movie = models.SessionMovie.get_first(db, filters={"hall_id": cur_model.hall_id,
                                                                  "date": cur_model.date,
                                                                  "time": cur_model.time})
    if db_session_movie:
        raise HTTPException(status_code=404, detail="SessionMovie already exist")

    try:
        cur_record = models.SessionMovie(**cur_model.dict())
        cur_record.save(db)
    except Exception:
        raise HTTPException(status_code=400, detail="SessionMovie already exist or not?")

    return RedirectResponse(f"/create_session/{movie_id}", status_code=status.HTTP_302_FOUND)
