from fastapi import Depends, HTTPException, Cookie, Request
from fastapi.security import SecurityScopes
from jose import jwt, ExpiredSignatureError, JWSError
from sqlalchemy.orm import Session
from starlette.templating import Jinja2Templates

from app import models as models, schemas as schemas
from app.database import get_db
from config import settings

templates = Jinja2Templates(directory="app/templates")


def get_token(request: Request, Token: str = Cookie(default="")):
    """
    Get token from cookies
    """
    if not Token:
        raise HTTPException(
            status_code=401,
            detail="Not authorization",
        )

    return Token


def get_current_user(security_scopes: SecurityScopes,
                     token: str = Depends(get_token),
                     db: Session = Depends(get_db)):
    """
    Get user from payloads
    Args:
        security_scopes: permission roles
        token: token from cookies
        db: DB session

    Returns: model DB

    """

    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        user_email: str = payload.get("sub")

        token_scopes = payload.get("scopes", "")
        token_data = schemas.TokenData(email=user_email, scopes=token_scopes)

    except ExpiredSignatureError:
        raise HTTPException(
            status_code=401,
            detail="Token expired",
        )
    except JWSError:
        raise HTTPException(
            status_code=401,
            detail="Not authorization",
        )

    db_user = models.User.get_first(db, filters={"email": token_data.email})

    if token_data.scopes not in security_scopes.scopes:
        raise HTTPException(
            status_code=401,
            detail="Not enough permissions",
        )

    return db_user
