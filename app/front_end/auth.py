from fastapi import Depends, APIRouter, Request, Form, status
from fastapi.responses import HTMLResponse, RedirectResponse

from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..api.auth import authenticate_user, create_access_token, create_refresh_token
from ..database import get_db
from .dependencies import templates

router = APIRouter(tags=["front-end"])


@router.get("/login", response_class=HTMLResponse)
async def login_html(request: Request):
    """
    Login page
    """
    context = {"request": request,
               "message": "Please login"}
    return templates.TemplateResponse("login.html", context)


@router.post("/login", response_class=HTMLResponse)
async def login_html(request: Request,
                     email: str = Form(...),
                     password: str = Form(...),
                     db: Session = Depends(get_db)):
    """
    Log in with email password
    """
    db_user = authenticate_user(db, email, password)

    if not db_user:
        context = {"request": request,
                   "message": "Please input correct email, password and login"}
        return templates.TemplateResponse("login.html", context)

    access_token = create_access_token(data={"sub": db_user.email, "scopes": db_user.role.name})
    refresh_token = create_refresh_token(data={"sub": db_user.email})

    redirect_response = RedirectResponse("/", status_code=status.HTTP_302_FOUND)
    redirect_response.set_cookie(key="Token", value=access_token.access_token)
    redirect_response.set_cookie(key="RefreshToken", value=refresh_token.access_token)

    return redirect_response


@router.get("/sign_up", response_class=HTMLResponse)
async def sign_up_html(request: Request):
    """
    Sign up page
    """
    context = {"request": request,
               "message": "Please sign up"}
    return templates.TemplateResponse("sign_up.html", context)


@router.post("/sign_up", response_class=HTMLResponse)
async def sign_up_html(request: Request,
                       email: str = Form(...),
                       password: str = Form(...),
                       repeat_password: str = Form(...),
                       db: Session = Depends(get_db)):
    """
    Register the user
    """
    if password != repeat_password:
        context = {"request": request,
                   "message": "Passwords must be same. Please sign up"}
        return templates.TemplateResponse("sign_up.html", context)

    request_body = {
        "email": email,
        "name": email,
        "password": password,
        "role_id": 2,
    }

    cur_model = schemas.UserCreate(**request_body)

    db_user = models.User.get_first(db, filters={"email": cur_model.email})
    if db_user:
        return RedirectResponse("/", status_code=status.HTTP_302_FOUND)

    db_user = models.User(**cur_model.dict(exclude={"password"}))
    db_user.hash_password(cur_model.password)
    db_user.save(db)

    access_token = create_access_token(data={"sub": db_user.email, "scopes": db_user.role.name})
    refresh_token = create_refresh_token(data={"sub": db_user.email})

    redirect_response = RedirectResponse("/", status_code=status.HTTP_302_FOUND)
    redirect_response.set_cookie(key="Token", value=access_token.access_token)
    redirect_response.set_cookie(key="RefreshToken", value=refresh_token.access_token)

    return redirect_response
