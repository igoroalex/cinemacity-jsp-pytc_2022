from fastapi import Depends, APIRouter, Request, Form, Security
from fastapi.responses import HTMLResponse

from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from .dependencies import templates, get_current_user

router = APIRouter(tags=["front-end"])


@router.get("/my_account", response_class=HTMLResponse)
async def my_account_html(request: Request,
                          cur_user: models.User = Security(get_current_user, scopes=["viewer"]),
                          ):
    """
    Open page with account data
    """
    response = schemas.User.from_orm(cur_user)

    return templates.TemplateResponse("my_account.html", {"request": request, "response": response})


@router.post("/my_account", response_class=HTMLResponse)
async def change_password_html(request: Request,
                               email: str = Form(...),
                               cur_user: models.User = Security(get_current_user, scopes=["viewer"]),
                               db: Session = Depends(get_db)):
    """
    Change password
    """
    cur_user.hash_password(email)
    cur_user.save(db)
    response = schemas.User.from_orm(cur_user)

    return templates.TemplateResponse("my_account.html", {"request": request, "response": response})
