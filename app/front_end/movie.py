from datetime import time, date

from fastapi import Depends, APIRouter, Request, Form, status, HTTPException, Security
from fastapi.responses import HTMLResponse, RedirectResponse
from pydantic import parse_obj_as

from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from .dependencies import templates, get_current_user
from ..models import ImdbPlatform

router = APIRouter(tags=["front-end"])


@router.get("/", response_class=HTMLResponse)
async def read_movies_html(request: Request,
                           db: Session = Depends(get_db)):
    """
    Page with list of movies
    """
    response = parse_obj_as(list[schemas.MovieAlbum], models.Movie.get_all(db))

    return templates.TemplateResponse("movies_album.html", {"request": request, "response": response})


@router.get("/search_in_movies_album", response_class=HTMLResponse)
async def read_movies_html(request: Request,
                           search_for_filter: str,
                           db: Session = Depends(get_db)):
    """
    Page with list of movies
    """
    filters = dict()
    if search_for_filter:

        filters.update({"search_for_filter": search_for_filter})
        try:
            may_time = tuple(search_for_filter.split(":"))
            may_time = may_time + ("00", "00", "00")
            session_time = time(int(may_time[0]), int(may_time[1]), int(may_time[2]))
            if session_time:
                filters.update({"session_time": session_time})
                filters.pop("search_for_filter")
        finally:
            pass

        try:
            may_date = tuple(search_for_filter.split("-"))
            may_date = may_date + ("00", "00", "00")
            session_date = date(int(may_date[0]), int(may_date[1]), int(may_date[2]))
            if session_date:
                filters.update({"session_date": session_date})
                filters.pop("search_for_filter")
        finally:
            pass

    response = parse_obj_as(list[schemas.MovieAlbum], models.Movie.find_movies(db, filters=filters))
    return templates.TemplateResponse("movies_album.html", {"request": request, "response": response})


@router.get("/movie_card/{movie_id}", response_class=HTMLResponse)
async def read_movie_html(request: Request,
                          movie_id: str,
                          db: Session = Depends(get_db)):
    """
    Page with movie card
    """

    db_movie = models.Movie.get_first(db, filters={"id": movie_id})
    response = schemas.MovieCard.from_orm(db_movie)

    return templates.TemplateResponse("movie_card.html", {"request": request, "response": response})


@router.get("/create_movie", response_class=HTMLResponse)
async def create_movie_html(request: Request,
                            cur_user: models.User = Security(get_current_user, scopes=["admin"]),
                            ):
    """
    Create movie
    """
    return templates.TemplateResponse("movie_create.html", {"request": request})


@router.post("/create_movie", response_class=HTMLResponse)
async def create_movie_html(imdb_id: str = Form(...),
                            cur_user: models.User = Security(get_current_user, scopes=["admin"]),
                            db: Session = Depends(get_db)):
    """
    Create movie
    """

    request_body = {"imdb_id": imdb_id, "with_imdb": True}
    cur_model = schemas.MovieCreate(**request_body)

    cur_record = models.Movie.get_first(db, filters={"imdb_id": cur_model.imdb_id})
    if cur_record:
        raise HTTPException(status_code=400, detail="Movie already exist")

    cur_record = models.Movie(**cur_model.dict(exclude={"with_imdb"}))

    cur_record.save(db)
    if request_body["with_imdb"]:
        cur_record.save_from_imdb(db)

    return RedirectResponse("/", status_code=status.HTTP_302_FOUND)


@router.get("/search_movie", response_class=HTMLResponse)
async def search_movie_html(request: Request,
                            search_title: str,
                            cur_user: models.User = Security(get_current_user, scopes=["admin"]),
                            ):
    """
    Search movies on Imdb
    """
    response = ImdbPlatform.get_movies(search_title)

    return templates.TemplateResponse("movie_search.html",
                                      {"request": request,
                                       "response": response,
                                       "search_title": search_title})


@router.get("/statistic", response_class=HTMLResponse)
async def read_statistic(request: Request,
                         cur_user: models.User = Security(get_current_user, scopes=["admin"]),
                         db: Session = Depends(get_db)):
    """Get all movies with statistic"""

    movies_tickets_count = parse_obj_as(list[schemas.MovieWithTicketsCount],
                                        models.SessionMovie.get_movies_tickets_count(db))

    halls_occupancy = parse_obj_as(list[schemas.HallWithSessions],
                                   models.Hall.get_all(db))

    return templates.TemplateResponse("statistic.html",
                                      {"request": request,
                                       "movies_tickets_count": movies_tickets_count,
                                       "halls_occupancy": halls_occupancy})
