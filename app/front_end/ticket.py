from fastapi import Depends, APIRouter, Request, Security
from fastapi.responses import HTMLResponse

from sqlalchemy.orm import Session

import app.schemas as schemas
import app.models as models
from ..api.tickets import create_ticket
from ..database import get_db
from .dependencies import templates, get_current_user

router = APIRouter(tags=["front-end"])


@router.get("/buy_ticket/{session_movie_id}/{seat}", response_class=HTMLResponse)
async def create_ticket_html(request: Request,
                             session_movie_id: int,
                             seat: int,
                             cur_user: models.User = Security(get_current_user, scopes=["viewer"]),
                             db: Session = Depends(get_db)):
    """
    Buy ticket
    """
    request_body = {"user_id": cur_user.id, "session_movie_id": session_movie_id, "seat": seat}
    cur_model = schemas.TicketCreate(**request_body)

    create_ticket(cur_model, db)

    return templates.TemplateResponse("ticket_bought.html", {"request": request})
