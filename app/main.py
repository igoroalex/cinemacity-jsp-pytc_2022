"""
Controller of app
"""
from pathlib import Path

from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

from config import settings
import app.api as api
import app.front_end as front
from app.database import engine, Base

Base.metadata.create_all(bind=engine)

app = FastAPI()

# app.mount("/app/static", StaticFiles(directory="app/static"), name="static")
app.mount("/app/static", StaticFiles(directory=Path(__file__).parent.parent.absolute()), name="static")


def configure():
    """
    Primary setup of app, Main FastAPI server instance
    """
    configure_app()
    configure_api()
    configure_front_end()


def configure_app():
    """
    Setup of common parameters of app
    """
    description = """
CinemaCityApp API helps CRUD instances. 🚀
    """

    tags_metadata = [
        {
            "name": "sessions movie",
            "description": "Operations with session hall.",
        },
        {
            "name": "movies",
            "description": "Operations with movie. When movie created persons and genres also created",
        },
        {
            "name": "users",
            "description": "Operations with users.",
        },
        {
            "name": "roles",
            "description": "Operations with user's roles.",
        },
        {
            "name": "imdb",
            "description": "Search movies by name on Imdb",
        },
        {
            "name": "tickets",
            "description": "Operations with user's tickets on current movie session ",
        },
        {
            "name": "auth",
            "description": "Operations with authorization and token stuff.",
        },
        {
            "name": "halls",
            "description": "Operations with halls.",
        },
    ]

    app.title = settings.APP_NAME
    app.description = description
    app.version = "0.0.2"
    app.openapi_tags = tags_metadata
    app.contact = {
        "name": "Oleksii Horodnychyi",
        "email": "igoroalex@gmail.com",
    }


def configure_api():
    """
    Add routers to app
    """
    app.include_router(api.sessions_movie_router)
    app.include_router(api.halls_router)
    app.include_router(api.users_router)
    app.include_router(api.roles_router)
    app.include_router(api.imdb_router)
    app.include_router(api.movies_router)
    app.include_router(api.tickets_router)
    app.include_router(api.auth_router)


def configure_front_end():
    """
    Add front_end routers to app
    """
    app.include_router(front.movie_router)
    app.include_router(front.ticket_router)
    app.include_router(front.auth_router)
    app.include_router(front.user_router)
    app.include_router(front.session_movie_router)


configure()
