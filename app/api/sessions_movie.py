from datetime import date, time

from fastapi import Depends, HTTPException, APIRouter, Security
from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from app.api.dependencies import PaginationParameters, get_current_active_user

router = APIRouter(
    prefix="/api/sessions_movie",
    tags=["sessions movie"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/", response_model=list[schemas.SessionMovie])
def read_session_movies(pagination: PaginationParameters = Depends(), db: Session = Depends(get_db)):
    """
    Get all session_movies
    """
    return models.SessionMovie.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/find",
            name="Find sessions movie with seats by filter",
            response_model=list[schemas.SessionMovieSeats])
def find_session_movies(pagination: PaginationParameters = Depends(),
                        db: Session = Depends(get_db),
                        search_for_filter: str | None = None,
                        sort_by_sate: bool = False
                        ):
    """
    Get all session_movies
        - **name_for_filter** - title, genre, actor, director, date_session, time_session
    """
    filters = dict()
    if search_for_filter:

        filters.update({"name_for_filter": search_for_filter})

        try:
            may_time = tuple(search_for_filter.split(":"))
            may_time = may_time + ("00", "00", "00")
            session_time = time(int(may_time[0]), int(may_time[1]), int(may_time[2]))
            print(session_time)
            if session_time:
                filters.update({"session_time": session_time})
                filters.pop("name_for_filter")
        finally:
            pass
        try:
            may_date = tuple(search_for_filter.split("-"))
            may_date = may_date + ("00", "00", "00")
            session_date = date(int(may_date[0]), int(may_date[1]), int(may_date[2]))
            print(session_date)
            if session_date:
                filters.update({"session_date": session_date})
                filters.pop("name_for_filter")
        finally:
            pass

    return models.SessionMovie.get_session_movies(db,
                                                  filters=filters,
                                                  skip=pagination.skip,
                                                  limit=pagination.limit,
                                                  sort_by_date=sort_by_sate)


@router.get("/{session_movie_id}", response_model=schemas.SessionMovie)
def read_session_movie(session_movie_id: int, db: Session = Depends(get_db)):
    """
    Get session_movie by id
    """
    db_session_movie = models.SessionMovie.get_first(db, filters={"id": session_movie_id})
    if db_session_movie is None:
        raise HTTPException(status_code=404, detail="SessionMovie not found")
    return db_session_movie


@router.post("/", response_model=schemas.SessionMovie, status_code=201)
def create_session_movie(session_movie: schemas.SessionMovieCreate, db: Session = Depends(get_db)):
    """
    Create new session_movie
    """
    db_hall = models.Hall.get_first(db, filters={"id": session_movie.hall_id})
    if db_hall is None:
        raise HTTPException(status_code=404, detail="Hall not found")

    db_movie = models.Movie.get_first(db, filters={"id": session_movie.movie_id})
    if db_movie is None:
        raise HTTPException(status_code=404, detail="Movie not found")

    db_session_movie = models.SessionMovie(**session_movie.dict())
    return db_session_movie.save(db)


@router.patch("/{session_movie_id}", response_model=schemas.SessionMovie)
def patch_session_movie(session_movie_id: int, user: schemas.SessionMoviePatch, db: Session = Depends(get_db)):
    """
    Patch session_movie's data by id
    """
    db_session_movie = models.SessionMovie.get_first(db, filters={"id": session_movie_id})
    if db_session_movie is None:
        raise HTTPException(status_code=404, detail="SessionMovie not found")
    return db_session_movie.change(db, user.dict())


@router.delete("/{session_movie_id}", response_model=schemas.SessionMovie)
def delete_session_movie(session_movie_id: int, db: Session = Depends(get_db)):
    """
    Delete session_movie by id
    """
    db_session_movie = models.SessionMovie.get_first(db, filters={"id": session_movie_id})
    if db_session_movie is None:
        raise HTTPException(status_code=404, detail="SessionMovie not found")
    return db_session_movie.delete(db)


@router.get("/seats/{session_movie_id}",
            name="Read seats of session movie",
            response_model=schemas.SessionMovieSeats)
def read_seats_session_movie(session_movie_id: int, db: Session = Depends(get_db)):
    """
    Get seats session_movie by id
    """
    db_session_movie = models.SessionMovie.get_first(db, filters={"id": session_movie_id})
    if db_session_movie is None:
        raise HTTPException(status_code=404, detail="SessionMovie not found")
    return db_session_movie
