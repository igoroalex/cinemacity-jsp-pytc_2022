from datetime import timedelta, datetime

from fastapi import Depends, HTTPException, APIRouter
from fastapi.security import OAuth2PasswordRequestForm
from jose import jwt
from sqlalchemy.orm import Session

import app.schemas as schemas
from config import settings
from .. import models
from ..database import get_db

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)


@router.post("/token", response_model=schemas.Token)
def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(),
                           db: Session = Depends(get_db)) -> schemas.Token:
    """
    Create a real JWT access token and return it.
    """
    cur_user = authenticate_user(db, form_data.username, form_data.password)
    if not cur_user:
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    cur_user = schemas.UserWithAuth.from_orm(cur_user)
    return create_access_token(data={"sub": cur_user.email, "scopes": cur_user.role.name})


def authenticate_user(db: Session, email: str, password: str) -> bool | models.User:
    """Check if exist user in DB and correct password.

    Args:
        db (Session): DB session
        email (str): filter for query from DB
        password (str): user's password

    Returns:
         bool: False if user not find or password not verify
         app.sql_app.user.User: user by email
    """
    db_user = models.User.get_first(db, filters={"email": email})
    if not db_user:
        return False
    if not db_user.verify_password(password):
        return False

    return db_user


def create_access_token(data: dict) -> schemas.Token:
    """Generate new access token

    Args:
        data (dict): data is input in token

    Returns:
        str: new access token
    """
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)

    return schemas.Token(access_token=encoded_jwt, token_type="bearer")


def create_refresh_token(data: dict) -> schemas.Token:
    """Generate new access token

    Args:
        data (dict): data is input in token

    Returns:
        str: new access token
    """
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(days=settings.REFRESH_TOKEN_EXPIRE_DAYS)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)

    return schemas.Token(access_token=encoded_jwt, token_type="refresh")
