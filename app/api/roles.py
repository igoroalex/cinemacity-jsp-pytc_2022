from fastapi import Depends, HTTPException, APIRouter, Security
from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from app.api.dependencies import PaginationParameters, get_current_active_user

router = APIRouter(
    prefix="/api/roles",
    tags=["roles"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/", response_model=list[schemas.Role])
def read_roles(pagination: PaginationParameters = Depends(), db: Session = Depends(get_db)):
    """
    Get all roles with pagination
    """
    return models.Role.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/{role_id}", response_model=schemas.Role)
def read_role(role_id: int, db: Session = Depends(get_db)):
    """
    Get role by id
    """
    db_role = models.Role.get_first(db, filters={"id": role_id})
    if db_role is None:
        raise HTTPException(status_code=404, detail="role not found")
    return db_role


@router.post("/", response_model=schemas.Role, status_code=201)
def create_role(role: schemas.RoleCreate, db: Session = Depends(get_db)):
    """
    Create new role
    """
    db_role = models.Role.get_first(db, filters={"name": role.name})
    if db_role:
        raise HTTPException(status_code=400, detail="Role already registered")
    db_role = models.Role(**role.dict())
    return db_role.save(db)


@router.patch("/{role_id}", response_model=schemas.Role)
def patch_role(role_id: int, role: schemas.RolePatch, db: Session = Depends(get_db)):
    """
    Patch role's data by id
    """
    db_role = models.Role.get_first(db, filters={"id": role_id})
    if db_role is None:
        raise HTTPException(status_code=404, detail="role not found")
    return db_role.change(db, role.dict())


@router.delete("/{role_id}", response_model=schemas.Role)
def delete_role(role_id: int, db: Session = Depends(get_db)):
    """
    Delete role by id
    """
    db_role = models.Role.get_first(db, filters={"id": role_id})
    if db_role is None:
        raise HTTPException(status_code=404, detail="role not found")
    return db_role.delete(db)
