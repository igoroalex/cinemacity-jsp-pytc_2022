from fastapi import Depends, HTTPException, APIRouter, Security
from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from app.api.dependencies import PaginationParameters, get_current_active_user

router = APIRouter(
    prefix="/api/users",
    tags=["users"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/", response_model=list[schemas.User])
def read_users(pagination: PaginationParameters = Depends(),
               db: Session = Depends(get_db)):
    """
    Get all users with pagination
    """
    return models.User.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/{user_id}", response_model=schemas.UserCreated)
def read_user(user_id: int, db: Session = Depends(get_db)):
    """
    Get user by id
    """
    db_user = models.User.get_first(db, filters={"id": user_id})
    if db_user is None:
        raise HTTPException(status_code=404, detail="user not found")
    return db_user


@router.post("/", response_model=schemas.UserCreated, status_code=201)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    """
    Create new user
    """
    db_user = models.User.get_first(db, filters={"email": user.email})
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    db_user = models.User(**user.dict(exclude={"password"}))
    db_user.hash_password(user.password)
    return db_user.save(db)


@router.patch("/{user_id}", response_model=schemas.UserWithAuth)
def patch_user(user_id: int, user: schemas.UserPatch, db: Session = Depends(get_db)):
    """
    Patch user's data by id
    """
    db_user = models.User.get_first(db, filters={"id": user_id})

    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    if user.password:
        db_user.hash_password(user.password)

    return db_user.change(db, user.dict(exclude={"password"}))


@router.delete("/{user_id}", response_model=schemas.User)
def delete_user(user_id: int, db: Session = Depends(get_db)):
    """
    Delete user by id
    """
    db_user = models.User.get_first(db, filters={"id": user_id})
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user.delete(db)
