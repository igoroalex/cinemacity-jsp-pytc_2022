from fastapi import APIRouter, Security

from .dependencies import get_current_active_user
from ..models import ImdbPlatform
from ..schemas import Imdb, ImdbTitle

router = APIRouter(
    prefix="/api/imdb",
    tags=["imdb"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/movies", response_model=list[ImdbTitle])
def find_movies(title: str):
    """
    Find all movies from Imdb by title
    """
    return ImdbPlatform.get_movies(title)


@router.get("/movie", response_model=Imdb)
def get_movie(imdb_id: str):
    """
    Get movie from Imdb by imdbID
    """
    return ImdbPlatform.get_movie(imdb_id)
