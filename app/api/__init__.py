from .users import router as users_router
from .tickets import router as tickets_router
from .sessions_movie import router as sessions_movie_router
from .roles import router as roles_router
from .movies import router as movies_router
from .imdb import router as imdb_router
from .halls import router as halls_router
from .auth import router as auth_router
