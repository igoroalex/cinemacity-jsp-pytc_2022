from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jose import jwt, JWTError
from pydantic import ValidationError
from sqlalchemy.orm import Session

from app import models, schemas
from app.database import get_db
from config import settings

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/token")


class PaginationParameters:
    """
    Pagination parameters for depends
    """
    def __init__(self, skip: int = 0, limit: int = 100):
        self.skip = skip
        self.limit = limit


def get_current_user(security_scopes: SecurityScopes,
                     token: str = Depends(oauth2_scheme),
                     db: Session = Depends(get_db)) -> schemas.UserWithAuth | HTTPException:
    """Decode the received token, verify it, and return the current user.

    If the token is invalid, return an HTTP error right away.

    Args:
        db: DB session
        security_scopes (SecurityScopes): scopes for access to endpoint
        token (str): Authentication bearer token

    Returns:
         app.sql_app.user.User: user by email
    """
    if security_scopes.scopes:
        authenticate_value = f'Bearer scope="{security_scopes.scopes}"'
    else:
        authenticate_value = f"Bearer"

    credentials_exception = HTTPException(
        status_code=401,
        detail="Could not validate credentials1",
        headers={"WWW-Authenticate": authenticate_value},
    )
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        user_email: str = payload.get("sub")
        if not user_email:
            raise credentials_exception

        token_scopes = payload.get("scopes", "")
        token_data = schemas.TokenData(email=user_email, scopes=token_scopes)

    except (JWTError, ValidationError):
        raise credentials_exception

    db_user = models.User.get_first(db, filters={"email": token_data.email})
    if not db_user:
        raise credentials_exception

    check_scopes(security_scopes, token_data, authenticate_value)

    return schemas.UserWithAuth.from_orm(db_user)


def check_scopes(security_scopes: SecurityScopes,
                 token_data: schemas.TokenData,
                 authenticate_value: str):
    """
    Check permission via scopes from token_data and security_scopes

    Args:
        security_scopes (SecurityScopes):
        token_data (schemas.TokenData):
        authenticate_value (str):

    Returns: raise Not enough permissions if there is
    """
    if token_data.scopes not in security_scopes.scopes:
        raise HTTPException(
            status_code=401,
            detail="Not enough permissions",
            headers={"WWW-Authenticate": authenticate_value},
        )


def get_current_active_user(
        cur_user: schemas.UserWithAuth = Depends(get_current_user)) -> schemas.UserWithAuth:
    """Check if user is active

    Args:
        cur_user: user to check

    Returns:
        app.sql_app.user.User: active user

    Raises:
        HTTPException: Inactive user
    """
    if not cur_user.is_active:
        raise HTTPException(status_code=401, detail="Inactive user")
    return cur_user
