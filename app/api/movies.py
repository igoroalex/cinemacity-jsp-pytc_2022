from datetime import time, date

from fastapi import Depends, HTTPException, APIRouter, Security
from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from app.api.dependencies import PaginationParameters, get_current_active_user

router = APIRouter(
    prefix="/api/movies",
    tags=["movies"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/", response_model=list[schemas.Movie])
def read_movies(pagination: PaginationParameters = Depends(),
                db: Session = Depends(get_db)):
    """
    Get all movies with pagination
    """
    return models.Movie.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/with_sessions", response_model=list[schemas.MovieAPIWithSessions])
def read_movies_with_sessions(pagination: PaginationParameters = Depends(),
                              db: Session = Depends(get_db)):
    """
    Get all movies with sessions
    """
    return models.Movie.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/tickets_count",
            name="Read movies with count tickets",
            response_model=list[schemas.MovieWithTicketsCount])
def read_movies_tickets_count(db: Session = Depends(get_db)):
    """
    Get movies with statistic
    """
    return models.SessionMovie.get_movies_tickets_count(db)


@router.get("/find",
            name="Find sessions movie with seats by filter",
            response_model=list[schemas.MovieAlbum])
def find_movies_with_sessions(pagination: PaginationParameters = Depends(),
                              db: Session = Depends(get_db),
                              search_for_filter: str | None = None,
                              sort_by_sate: bool = False
                              ):
    """
    Get all session_movies
        - **name_for_filter** - title, genre, actor, director, date_session, time_session
    """
    filters = dict()
    if search_for_filter:

        filters.update({"search_for_filter": search_for_filter})

        try:
            may_time = tuple(search_for_filter.split(":"))
            may_time = may_time + ("00", "00", "00")
            session_time = time(int(may_time[0]), int(may_time[1]), int(may_time[2]))
            if session_time:
                filters.update({"session_time": session_time})
                filters.pop("search_for_filter")

        finally:
            pass
        try:
            may_date = tuple(search_for_filter.split("-"))
            may_date = may_date + ("00", "00", "00")
            session_date = date(int(may_date[0]), int(may_date[1]), int(may_date[2]))
            if session_date:
                filters.update({"session_date": session_date})
                filters.pop("search_for_filter")
        finally:
            pass

    return models.Movie.find_movies(db,
                                    filters=filters,
                                    skip=pagination.skip,
                                    limit=pagination.limit)


@router.get("/{movie_id}", response_model=schemas.Movie)
def read_movie(movie_id: int, db: Session = Depends(get_db)):
    """
    Get movie by id
    """
    db_movie = models.Movie.get_first(db, filters={"id": movie_id})
    if db_movie is None:
        raise HTTPException(status_code=404, detail="Movie not found")
    return db_movie


@router.post("/", response_model=schemas.MovieCreated, status_code=201)
def create_movie(movie: schemas.MovieCreate, db: Session = Depends(get_db)):
    """
    Create new movie
    """
    db_movie = models.Movie.get_first(db, filters={"imdb_id": movie.imdb_id})
    if db_movie:
        db_movie.save_from_imdb(db)
        raise HTTPException(status_code=400, detail="Movie already exist")
    db_movie = models.Movie(**movie.dict(exclude={"with_imdb"}))
    db_movie.save(db)

    if movie.with_imdb:
        db_movie.save_from_imdb(db)

    return db_movie


@router.patch("/{movie_id}", response_model=schemas.Movie)
def patch_movie(movie_id: int, movie: schemas.MoviePatch, db: Session = Depends(get_db)):
    """
    Patch movie's data by id
    """
    db_movie = models.Movie.get_first(db, filters={"id": movie_id})
    if db_movie is None:
        raise HTTPException(status_code=404, detail="Movie not found")
    return db_movie.change(db, movie.dict())


@router.delete("/{movie_id}", response_model=schemas.Movie)
def delete_movie(movie_id: int, db: Session = Depends(get_db)):
    """
    Delete movie by id
    """
    db_movie = models.Movie.get_first(db, filters={"id": movie_id})
    if db_movie is None:
        raise HTTPException(status_code=404, detail="Movie not found")
    return db_movie.delete(db)
