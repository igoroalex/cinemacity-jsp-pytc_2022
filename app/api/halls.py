from fastapi import Depends, HTTPException, APIRouter, Security
from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from app.api.dependencies import PaginationParameters, get_current_active_user

router = APIRouter(
    prefix="/api/halls",
    tags=["halls"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/", response_model=list[schemas.Hall])
def read_halls(pagination: PaginationParameters = Depends(), db: Session = Depends(get_db)):
    """
    Get all halls
    """
    return models.Hall.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/with_sessions", response_model=list[schemas.HallWithSessions])
def read_halls_with_sessions(pagination: PaginationParameters = Depends(), db: Session = Depends(get_db)):
    """
    Get halls with occupancy
    """
    return models.Hall.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/{hall_id}", response_model=schemas.Hall)
def read_hall(hall_id: int, db: Session = Depends(get_db)):
    """
    Get hall by id
    """
    db_hall = models.Hall.get_first(db, filters={"id": hall_id})
    if db_hall is None:
        raise HTTPException(status_code=404, detail="Hall not found")
    return db_hall


@router.post("/", response_model=schemas.Hall, status_code=201)
def create_hall(hall: schemas.HallCreate, db: Session = Depends(get_db)):
    """
    Create new hall
    """
    db_hall = models.Hall(**hall.dict())
    return db_hall.save(db)


@router.patch("/{hall_id}", response_model=schemas.Hall)
def patch_hall(hall_id: int, user: schemas.HallPatch, db: Session = Depends(get_db)):
    """
    Patch hall's data by id
    """
    db_hall = models.Hall.get_first(db, filters={"id": hall_id})
    if db_hall is None:
        raise HTTPException(status_code=404, detail="Hall not found")
    return db_hall.change(db, user.dict())


@router.delete("/{hall_id}", response_model=schemas.Hall)
def delete_hall(hall_id: int, db: Session = Depends(get_db)):
    """
    Delete hall by id
    """
    db_hall = models.Hall.get_first(db, filters={"id": hall_id})
    if db_hall is None:
        raise HTTPException(status_code=404, detail="Hall not found")
    return db_hall.delete(db)
