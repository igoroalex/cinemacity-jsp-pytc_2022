from fastapi import Depends, HTTPException, APIRouter, Security
from sqlalchemy.orm import Session

import app.models as models
import app.schemas as schemas
from ..database import get_db
from app.api.dependencies import PaginationParameters, get_current_active_user

router = APIRouter(
    prefix="/api/tickets",
    tags=["tickets"],
    dependencies=[Security(get_current_active_user, scopes=["admin"])],
)


@router.get("/", response_model=list[schemas.Ticket])
def read_tickets(pagination: PaginationParameters = Depends(), db: Session = Depends(get_db)):
    """
    Get all tickets
    """
    return models.Ticket.get_all(db, skip=pagination.skip, limit=pagination.limit)


@router.get("/{ticket_id}", response_model=schemas.Ticket)
def read_ticket(ticket_id: int, db: Session = Depends(get_db)):
    """
    Get ticket by id
    """
    db_ticket = models.Ticket.get_first(db, filters={"id": ticket_id})
    if db_ticket is None:
        raise HTTPException(status_code=404, detail="Ticket not found")

    return db_ticket


@router.post("/", response_model=schemas.Ticket, status_code=201)
def create_ticket(
        ticket: schemas.TicketCreate, db: Session = Depends(get_db)
):
    """
    Create new ticket for user by id
    """
    db_session_movie = models.SessionMovie.get_first(db, filters={"id": ticket.session_movie_id})
    if db_session_movie is None:
        raise HTTPException(status_code=404, detail="SessionMovie not found")

    db_user = models.SessionMovie.get_first(db, filters={"id": ticket.user_id})
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    session_movie_seats = schemas.SessionMovieSeats.from_orm(db_session_movie)

    if ticket.seat in session_movie_seats.ticket_seats:
        raise HTTPException(status_code=409, detail="Seat is already taken on the SessionMovie")

    db_ticket = models.Ticket(**ticket.dict())
    return db_ticket.save(db)


@router.patch("/{ticket_id}", response_model=schemas.Ticket)
def patch_ticket(ticket_id: int, user: schemas.TicketPatch, db: Session = Depends(get_db)):
    """
    Patch ticket's data by id
    """
    db_ticket = models.Ticket.get_first(db, filters={"id": ticket_id})
    if db_ticket is None:
        raise HTTPException(status_code=404, detail="Ticket not found")
    return db_ticket.change(db, user.dict())


@router.delete("/{ticket_id}", response_model=schemas.Ticket)
def delete_ticket(ticket_id: int, db: Session = Depends(get_db)):
    """
    Delete ticket by id
    """
    db_ticket = models.Ticket.get_first(db, filters={"id": ticket_id})
    if db_ticket is None:
        raise HTTPException(status_code=404, detail="Ticket not found")
    return db_ticket.delete(db)


@router.patch("/pay/{ticket_id}", response_model=schemas.Ticket)
def pay_ticket(ticket_id: int, user: schemas.TicketPay, db: Session = Depends(get_db)):
    """
    Patch ticket's data by id
    """
    db_ticket = models.Ticket.get_first(db, filters={"id": ticket_id})
    if db_ticket is None:
        raise HTTPException(status_code=404, detail="Ticket not found")
    return db_ticket.change(user.dict())
