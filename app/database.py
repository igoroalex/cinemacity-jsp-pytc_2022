"""
Declare common SQL instances: engine, session, base
"""
from pydantic import BaseModel
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config import settings


engine = create_engine(settings.SQLALCHEMY_DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    """Generator Get connection to DB for Dependencies and not only.

    Returns:
        Connection to DB.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class BaseORM(BaseModel):
    """
    Class for schemas to convert from model DB
    """
    class Config:
        orm_mode = True


