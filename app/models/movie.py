from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, or_

from sqlalchemy.orm import relationship, Session

from .. import models, schemas
from ..database import Base
from .model_db import ModelDB


class CastMovie(Base, ModelDB):
    """
    Model many-to-many for movie's cast
    """
    __tablename__ = "cast_movie"

    movie_id = Column(Integer, ForeignKey("movies.id"), primary_key=True)
    person_id = Column(Integer, ForeignKey("persons.id"), primary_key=True)


class DirectorMovie(Base, ModelDB):
    """
    Model many-to-many for movie's directors
    """
    __tablename__ = "directors_movie"

    movie_id = Column(Integer, ForeignKey("movies.id"), primary_key=True)
    person_id = Column(Integer, ForeignKey("persons.id"), primary_key=True)


class GenreMovie(Base, ModelDB):
    """
    Model many-to-many for movie's genres
    """
    __tablename__ = "genres_movie"

    movie_id = Column(Integer, ForeignKey("movies.id"), primary_key=True)
    genre_id = Column(Integer, ForeignKey("genres.id"), primary_key=True)


class Movie(Base, ModelDB):
    """Movie DB model"""
    __tablename__ = "movies"

    id = Column(Integer, primary_key=True, index=True)
    imdb_id = Column(String, unique=True, index=True)
    title = Column(String(256))
    imdb_rating = Column(String)
    is_active = Column(Boolean, default=True)

    sessions_movie = relationship("SessionMovie", back_populates="movie_session")

    cast = relationship(
        "Person",
        secondary="cast_movie",
        back_populates="cast_movies")

    directors = relationship(
        "Person",
        secondary="directors_movie",
        back_populates="directors_movies")

    genres = relationship(
        "Genre",
        secondary="genres_movie",
        back_populates="genres_movies")

    def save_from_imdb(self, db: Session):
        """
        Create records persons, genres in DB from Imdb
        """
        movie = models.ImdbPlatform.get_movie(str(self.imdb_id))
        self.title = movie.title
        self.imdb_rating = movie.rating
        self.save(db)

        self._save_persons(db, movie)
        self._save_genres(db, movie)

    def _save_genres(self, db: Session, movie: schemas.Imdb):
        """
        Create records genres in DB from Imdb

        Args:
            movie (schemas.Imdb): get genres from movie
        """
        for genre in movie.genres:
            genre = genre.lower()
            if models.Genre.get_first(db, filters={"name": genre}):
                continue
            db_genre = models.Genre(name=genre)
            db_genre.save(db)

            request_body = {"movie_id": self.id, "genre_id": db_genre.id}

            cur_model = schemas.GenreMovie(**request_body)

            cur_record = models.GenreMovie(**cur_model.dict())
            cur_record.save(db)

    def _save_persons(self, db: Session, movie: schemas.Imdb):
        """
        Create records persons in DB from Imdb

        Args:
            movie (schemas.Imdb): get genres from movie
        """
        for person in movie.cast:
            if models.Person.get_first(db, filters={"name": person.name}):
                continue
            db_person = models.Person(name=person.name)
            db_person.save(db)

            request_body = {"movie_id": self.id, "person_id": db_person.id}
            cur_model = schemas.Cast(**request_body)

            cur_record = models.CastMovie(**cur_model.dict())
            cur_record.save(db)

        for person in movie.director:
            if models.Person.get_first(db, filters={"name": person.name}):
                continue
            db_person = models.Person(name=person.name)
            db_person.save(db)

            request_body = {"movie_id": self.id, "person_id": db_person.id}
            cur_model = schemas.Director(**request_body)

            cur_record = models.DirectorMovie(**cur_model.dict())
            cur_record.save(db)

    @classmethod
    def find_movies(cls, db: Session,
                    filters: dict = None,
                    skip: int = 0,
                    limit: int = 100,
                    ):
        """Get records from DB by filters with pagination

        Args:
            filters (dict, optional): filter for query from DB
            skip (int, optional): skip first elements
            limit (int, optional): max count elements
            db (Session): DB session

        Returns: list of Model DB
        """
        filters = filters if filters else dict()

        query = db.query(cls) \
            .filter_by(**{"is_active": True})

        if search_for_filter := filters.get("search_for_filter", None):
            query = query.filter(or_(cls.title.contains(search_for_filter),
                                     cls.genres.any(models.Genre.name.contains(search_for_filter)),
                                     cls.cast.any(models.Person.name.contains(search_for_filter)),
                                     cls.directors.any(models.Person.name.contains(search_for_filter)),
                                     ))
            filters.pop("search_for_filter")

        if session_time := filters.get("session_time", None):
            query = query.filter(cls.sessions_movie.any(models.SessionMovie.time == session_time))
            filters.pop("session_time")

        if session_date := filters.get("session_date", None):
            query = query.filter(cls.sessions_movie.any(models.SessionMovie.date == session_date))
            filters.pop("session_date")

        query = query.filter_by(**filters)

        query = query \
            .order_by(cls.id) \
            .offset(skip) \
            .limit(limit)

        return query.all()
