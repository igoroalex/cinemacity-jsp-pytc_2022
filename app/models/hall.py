from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from ..database import Base
from .model_db import ModelDB


class Hall(Base, ModelDB):
    """Hall DB model"""
    __tablename__ = "halls"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(32), unique=True, index=True)
    number_of_seats = Column(Integer, nullable=False)
    is_active = Column(Boolean, default=True)

    sessions_movie = relationship("SessionMovie", back_populates="hall_session")
