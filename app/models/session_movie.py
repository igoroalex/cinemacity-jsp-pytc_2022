from sqlalchemy import Column, Integer, Boolean, Date, Time, ForeignKey, func, or_, UniqueConstraint
from sqlalchemy.orm import relationship, Session

import app.models as models
from ..database import Base
from .model_db import ModelDB


class SessionMovie(Base, ModelDB):
    """Hall DB model"""
    __tablename__ = "sessions_movie"

    id = Column(Integer, primary_key=True, index=True)
    date = Column(Date, nullable=False, index=True)
    time = Column(Time, nullable=False)
    is_active = Column(Boolean, default=True)

    hall_id = Column(Integer, ForeignKey("halls.id"), nullable=False)
    hall_session = relationship("Hall")

    movie_id = Column(Integer, ForeignKey("movies.id"), nullable=False)
    movie_session = relationship("Movie")

    tickets = relationship("Ticket", back_populates="session_movie")

    __table_args__ = (UniqueConstraint('hall_id', 'date', "time", name='_hall_date_time_uc'),
                      )
    @classmethod
    def get_movies_tickets_count(cls, db: Session):
        """
        Get movies with count of tickets
        Args:
            db: DB session

        Returns: list of Model DB
        """
        subquery = db.query(cls.movie_id.label("movie_id"), func.count(models.Ticket.id).label("count_tickets")) \
            .join(models.Ticket, models.Ticket.session_movie_id == cls.id) \
            .filter_by(**{"is_active": True}) \
            .group_by(cls.movie_id) \
            .order_by(cls.movie_id) \
            .subquery()
        query = db.query(models.Movie, subquery.c.movie_id, subquery.c.count_tickets) \
            .join(models.Movie, subquery.c.movie_id == models.Movie.id) \
            .order_by(subquery.c.movie_id)

        return query.all()

    @classmethod
    def get_session_movies(cls, db: Session,
                           filters: dict = None,
                           skip: int = 0,
                           limit: int = 100,
                           sort_by_date: bool = False):
        """Get records from DB by filters with pagination

        Args:
            sort_by_date (bool): sort by date or not
            filters (dict, optional): filter for query from DB
            skip (int, optional): skip first elements
            limit (int, optional): max count elements
            db (Session): DB session

        Returns: list of Model DB
        """
        filters = filters if filters else dict()

        query = db.query(cls) \
            .filter_by(**{"is_active": True})

        if search_for_filter := filters.get("search_for_filter", None):
            query = query.filter(cls.movie_session.has(
                or_(models.Movie.title == search_for_filter,
                    models.Movie.genres.any(models.Genre.name.contains(search_for_filter)),
                    models.Movie.cast.any(models.Person.name.contains(search_for_filter)),
                    models.Movie.directors.any(models.Person.name.contains(search_for_filter)),
                    )))
            filters.pop("search_for_filter")

        if hall_id := filters.get("hall_id", None):
            query = query.filter(cls.hall_session.has(models.Hall.id == hall_id))
            filters.pop("hall_id")

        if session_time := filters.get("session_time", None):
            query = query.filter(cls.time == session_time)
            filters.pop("session_time")

        if session_date := filters.get("session_date", None):
            query = query.filter(cls.date == session_date)
            filters.pop("session_date")

        query = query.filter_by(**filters)
        if sort_by_date:
            query = query.order_by(cls.date.desc())

        query = query \
            .order_by(cls.id) \
            .offset(skip) \
            .limit(limit)

        return query.all()
