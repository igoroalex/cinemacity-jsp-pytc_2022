from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from passlib.context import CryptContext

from ..database import Base
from .model_db import ModelDB

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class User(Base, ModelDB):
    """User DB model"""
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)
    email = Column(String(64), unique=True, index=True)
    name = Column(String(128))
    hashed_password = Column(String)

    tickets = relationship("Ticket", back_populates="user")

    role_id = Column(Integer, ForeignKey("roles.id"), nullable=False)
    role = relationship("Role", back_populates="users")

    def hash_password(self, password: str):
        """
        A utility function to hash a password coming from the user

        Args:
            password (str): user's password
        """
        if password:
            self.hashed_password = pwd_context.hash(password)

    def verify_password(self, plain_password: str) -> bool:
        """
        Verify if a received password matches the hash stored

        Args:
            plain_password (str): entered password

        Returns:
            bool: bool result if verify or not

        """
        return pwd_context.verify(plain_password, str(self.hashed_password))
