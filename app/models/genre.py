from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from ..database import Base
from .model_db import ModelDB


class Genre(Base, ModelDB):
    """Genre DB model"""
    __tablename__ = "genres"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(64), unique=True, index=True)
    is_active = Column(Boolean, default=True)

    genres_movies = relationship(
        "Movie",
        secondary="genres_movie",
        back_populates="genres")
