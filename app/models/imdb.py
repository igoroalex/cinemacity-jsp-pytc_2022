from imdb import Cinemagoer

from ..schemas import Imdb, ImdbTitle

ia = Cinemagoer()


class ImdbPlatform:
    """
    Class for getting data about movies, persons, genres from Imdb
    """

    @staticmethod
    def get_movie(imdb_id: str) -> Imdb:
        """
        Get a lot of data about movie and draw out though schema

        Args:
            imdb_id (str): id movie

        Returns: schema
        """
        return Imdb(**ia.get_movie(imdb_id))

    @staticmethod
    def get_movies(name: str):
        """
        Search movies by name

        Args:
            name (str): movie name

        Returns: tuple of movies
        """
        return (ImdbTitle(imdbID=m.movieID, year=m.get("year", None), title=m["title"]) for m in ia.search_movie(name))
