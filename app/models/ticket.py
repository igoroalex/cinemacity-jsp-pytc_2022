from sqlalchemy import Column, Integer, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy.orm import relationship

from ..database import Base
from .model_db import ModelDB


class Ticket(Base, ModelDB):
    """Ticket DB model"""
    __tablename__ = "tickets"

    id = Column(Integer, primary_key=True, index=True)
    is_active = Column(Boolean, default=True)
    seat = Column(Integer, nullable=False)

    user_id = Column(Integer, ForeignKey("users.id"), index=True)
    user = relationship("User", back_populates="tickets")

    session_movie_id = Column(Integer, ForeignKey("sessions_movie.id"))
    session_movie = relationship("SessionMovie", back_populates="tickets")

    paid = Column(Boolean, default=False)

    __table_args__ = (UniqueConstraint('session_movie_id', 'seat', name='_session_movie_seat_uc'),
                      )
