from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session


class ModelDB:

    @classmethod
    def get_all(cls, db: Session, filters: dict = None, skip: int = 0, limit: int = 100):
        """
        Get all records from DB using Session with pagination

        Args:
            filters (dict, optional): filter for query from DB
            skip (int, optional): skip first elements
            limit (int, optional): max count elements
            db (Session): DB session

        Returns: list of Model DB
        """
        filters = filters if filters else dict()
        query = db.query(cls)\
            .filter_by(**{"is_active": True})\
            .filter_by(**filters)\
            .order_by(cls.id)\
            .offset(skip)\
            .limit(limit)

        return query.all()

    @classmethod
    def get_first(cls, db: Session, filters: dict = None):
        """
        Get first record from DB by filter using Session

        Args:
            filters (dict, optional): filter for query
            db (Session): DB session

        Returns: Model DB
        """
        query = db.query(cls).filter_by(**filters) if filters else db.query(cls)
        return query.first()

    def save(self, db: Session, add_new: bool = True):
        """
        Create record in DB using session.

        Returns: model DB
        """
        if add_new:
            db.add(self)

        try:
            db.commit()
        except IntegrityError:

            db.rollback()
            raise HTTPException(status_code=404, detail="bad request")

        if add_new:
            db.refresh(self)
        return self

    def change(self, db: Session, data: dict):
        """Change fields in model DB from data.

        Args:
            db (Session): DB session
            data: data for change the model DB

        Returns: model DB
        """
        for k, v in data.items():
            if v and hasattr(self, k):
                self.__setattr__(k, v)
        return self.save(db, add_new=False)

    def delete(self, db: Session):
        """
        Mark record for deleting

        Returns: model DB
        """
        self.is_active = False
        return self.save(db, add_new=False)
