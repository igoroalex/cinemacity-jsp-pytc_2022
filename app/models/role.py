from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from ..database import Base
from .model_db import ModelDB


class Role(Base, ModelDB):
    """
    Role DB Model, roles of User's Access
    """
    __tablename__ = "roles"

    id = Column(Integer, primary_key=True)
    is_active = Column(Boolean, default=True)
    name = Column(String(32), nullable=False, unique=True)

    users = relationship("User", back_populates="role")
