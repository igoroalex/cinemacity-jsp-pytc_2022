from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from ..database import Base
from .model_db import ModelDB


class Person(Base, ModelDB):
    """Person DB model"""
    __tablename__ = "persons"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(128), unique=True, index=True)
    is_active = Column(Boolean, default=True)

    cast_movies = relationship(
        "Movie",
        secondary="cast_movie",
        back_populates="cast")

    directors_movies = relationship(
        "Movie",
        secondary="directors_movie",
        back_populates="directors")
