from pydantic import BaseModel

from app.database import BaseORM


class Genre(BaseModel):
    name: str


class GenreInMovie(BaseORM):
    name: str
