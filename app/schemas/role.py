from pydantic import BaseModel

from app.database import BaseORM


class RoleBase(BaseModel):
    name: str


class RoleCreate(RoleBase):
    pass


class RoleDB(BaseModel):
    id: int
    is_active: bool


class Role(BaseORM,
           RoleBase,
           RoleDB,
           ):
    pass


class RolePatch(RoleCreate):
    name: str | None


class RoleInUser(BaseORM, RoleBase):
    pass
