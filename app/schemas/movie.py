from datetime import date, time

from pydantic import BaseModel, root_validator, Field

from app.database import BaseORM
from app.schemas.genre import GenreInMovie
from app.schemas.person import PersonInMovie
from app.schemas.session_movie import SessionMovieSeats


class MovieBase(BaseModel):
    title: str | None
    imdb_id: str
    imdb_rating: str | None


class MovieCreate(MovieBase):
    with_imdb: bool = False


class SessionInMovie(BaseORM):
    id: int
    date: date
    time: time
    hall_id: int


class MovieWithGenres(BaseModel):
    genres: list[GenreInMovie] = Field(exclude=True)
    genres_names: str = ""

    @root_validator
    def get_genres_name(cls, values):
        values["genres_names"] = ", ".join(i.name for i in values["genres"])
        return values


class MovieWithDirectors(BaseModel):
    directors: list[PersonInMovie] = Field(exclude=True)
    directors_names: str = ""

    @root_validator
    def get_directors_names(cls, values):
        values["directors_names"] = ", ".join(i.name for i in values["directors"])
        return values


class MovieWithCast(BaseModel):
    cast: list[PersonInMovie] = Field(exclude=True)
    cast_name: str = ""

    @root_validator
    def get_cast_name(cls, values):
        values["cast_name"] = ", ".join(i.name for i in values["cast"])
        return values


class MovieWithSessions(BaseModel):
    sessions_movie: list[SessionInMovie]
    sessions_movie_dates: str = ""

    @root_validator
    def get_sessions_movie_dates(cls, values):
        values["sessions_movie_dates"] = ", ".join(str(i.date) for i in values["sessions_movie"])
        return values


class MovieDB(BaseModel):
    id: int
    is_active: bool


class MovieCreated(MovieDB, BaseORM):
    pass


class MovieAPIWithSessions(MovieBase, BaseORM):
    sessions_movie: list[SessionInMovie]


class Movie(BaseORM,
            MovieWithCast,
            MovieWithDirectors,
            MovieWithGenres,
            MovieDB,
            MovieBase,
            ):
    pass


class MovieAlbum(BaseORM,
                 MovieDB,
                 MovieWithSessions,
                 MovieWithGenres,
                 MovieWithDirectors,
                 MovieBase,
                 ):
    pass


class MovieWithFreeSeats(MovieWithSessions):
    sessions_movie: list[SessionMovieSeats]


class MovieCard(MovieBase,
                BaseORM,
                MovieDB,
                MovieWithGenres,
                MovieWithCast,
                MovieWithDirectors,
                MovieWithFreeSeats,
                ):
    pass


class MoviePatch(BaseModel):
    title: str | None
    imdb_id: str | None
    imdb_rating: str | None


class Cast(BaseModel):
    movie_id: int
    person_id: int


class Director(BaseModel):
    movie_id: int
    person_id: int


class GenreMovie(BaseModel):
    movie_id: int
    genre_id: int
