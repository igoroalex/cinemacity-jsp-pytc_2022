from pydantic import BaseModel

from .person import Person


class ImdbBase(BaseModel):
    title: str
    imdbID: str
    year: int | None


class Imdb(ImdbBase):
    rating: float
    cast: list[Person] = []
    genres: list
    director: list[Person] = []


class ImdbTitle(ImdbBase):
    pass


class ImdbSearch(BaseModel):
    movies: list[ImdbBase]
