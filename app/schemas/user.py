from pydantic import BaseModel

from app.database import BaseORM
from app.schemas.role import RoleInUser
from app.schemas.ticket import TicketInUser


class UserBase(BaseModel):
    email: str
    name: str


class UserCreate(UserBase):
    password: str
    role_id: int


class UserCreated(BaseORM):
    id: int


class UserWithRole(UserBase):
    role: RoleInUser


class UserWithTickets(BaseModel):
    tickets: list[TicketInUser] = []


class UserDB(BaseModel):
    id: int
    is_active: bool


class UserWithAuth(BaseORM, UserDB, UserBase):
    role: RoleInUser


class User(BaseORM,
           # UserWithTickets,
           # UserWithRole,
           UserBase,
           UserDB):
    pass


class UserPatch(BaseModel):
    email: str | None
    name: str | None
    password: str | None
    role_id: int | None
