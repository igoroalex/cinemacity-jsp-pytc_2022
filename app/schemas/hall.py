from datetime import date, time

from pydantic import BaseModel, root_validator, Field

from app.database import BaseORM


class HallBase(BaseModel):
    name: str
    number_of_seats: int


class HallCreate(HallBase):
    pass


class HallDB(BaseModel):
    id: int
    is_active: bool


class HallCreated(BaseORM, HallDB):
    pass


class Hall(BaseORM,
           HallBase,
           HallDB,
           ):
    id: int
    is_active: bool


class HallPatch(HallBase):
    name: str | None


class TicketInSessionMovie(BaseORM):
    seat: int


class HallForSession(BaseORM):
    name: str
    number_of_seats: int


class SessionInHall(BaseModel):
    is_active: bool
    date: date
    time: time
    free_count: int = 0
    ticket_count: int = 0
    occupancy: int = 100
    hall_session: HallForSession = Field(exclude=True)
    tickets: list[TicketInSessionMovie] = Field(exclude=True)

    @root_validator
    def get_ticket_count(cls, values):
        values["ticket_count"] = len(values["tickets"])
        return values

    @root_validator
    def get_free_count(cls, values):
        values["free_count"] = values["hall_session"].number_of_seats - values["ticket_count"]
        return values

    @root_validator
    def get_occupancy(cls, values):
        values["occupancy"] = int(values["ticket_count"] / values["hall_session"].number_of_seats * 100)
        return values

    class Config:
        orm_mode = True


class HallWithSessions(BaseORM, HallBase):

    sessions_movie: list[SessionInHall]


class HallInSessionMovie(BaseORM):
    name: str
    number_of_seats: int
