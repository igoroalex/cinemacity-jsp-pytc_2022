from pydantic import BaseModel

from app.database import BaseORM
from app.schemas.session_movie import SessionMovieInTicket


class TicketBase(BaseModel):
    seat: int
    session_movie_id: int


class TicketCreate(TicketBase):
    user_id: int


class TicketPay(TicketCreate):
    paid: bool = True


class TicketDB(BaseModel):
    id: int
    is_active: bool


class Ticket(BaseORM,
             TicketPay,
             TicketDB,
             ):
    pass


class TicketPatch(TicketCreate):
    user_id: int | None
    seat: int | None
    session_movie_id: int | None
    paid: bool | None


class TicketInUser(BaseORM):
    id: int
    is_active: bool
    seat: int
    paid: bool
    session_movie: SessionMovieInTicket


class TicketInMovie(BaseORM):
    id: int
