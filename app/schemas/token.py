from pydantic import BaseModel


class Token(BaseModel):
    """
    Schema that will be used when reading data, when returning it from the API,
    used in the token endpoint for the response
    """
    access_token: str
    token_type: str


class TokenData(BaseModel):
    """
    Data from token
    """
    email: str
    scopes: str
