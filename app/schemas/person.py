from pydantic import BaseModel

from app.database import BaseORM


class Person(BaseModel):
    name: str


class PersonInMovie(BaseORM):
    name: str
