from datetime import date, time
from typing import Optional

from pydantic import BaseModel, root_validator, Field

from app.database import BaseORM
from app.schemas.hall import HallInSessionMovie


class SessionMovieBase(BaseModel):
    date: date
    time: time


class SessionMoviSearch(BaseModel):
    date: date | None
    time: time | None


class SessionMovieWithDatetime(SessionMovieBase):
    datetime_repr: str = ""

    @root_validator
    def get_datetime_repr(cls, values):
        values["datetime_repr"] = f"{values['date']} {values['time']}"
        return values


class SessionMovieCreate(SessionMovieBase):
    hall_id: int
    movie_id: int


class SessionMovieWithHall(BaseModel):
    hall_session: HallInSessionMovie


class MovieInSessionMovie(BaseORM):
    title: str


class SessionMovieWithMovie(BaseModel):
    movie_session: MovieInSessionMovie


class SessionMovieInTicket(BaseORM,
                           SessionMovieWithDatetime,
                           SessionMovieWithMovie):
    pass


class TicketInSessionMovie(BaseORM):
    seat: int


class SessionMovieWithTickets(SessionMovieWithHall):
    tickets: list[TicketInSessionMovie] = Field(exclude=True)
    all_seats: tuple[int] = Field(tuple(), exclude=True)
    ticket_seats: tuple[int] = ()
    free_seats: tuple[int] = ()

    @root_validator
    def get_all_seats(cls, values):
        seats = (i + 1 for i in range(values["hall_session"].number_of_seats))
        values["all_seats"] = tuple(seats)
        return values

    @root_validator
    def get_ticket_seats(cls, values):
        seats = (t.seat for t in values["tickets"])
        values["ticket_seats"] = tuple(seats)
        return values

    @root_validator
    def get_free_seats(cls, values):
        seats = (t.seat for t in values["tickets"])
        values["free_seats"] = set(seats) ^ set(values["all_seats"])
        return values


class SessionMovieDB(BaseModel):
    id: int
    is_active: bool


class SessionMovie(BaseORM,
                   # SessionMovieWithTickets,
                   SessionMovieWithMovie,
                   SessionMovieWithHall,
                   SessionMovieWithDatetime,
                   SessionMovieBase,
                   SessionMovieDB,
                   ):
    pass


class SessionMoviePatch(SessionMovieBase):
    date: Optional[date]
    time: Optional[time]


class SessionMovieSeats(BaseORM,
                        SessionMovieWithTickets,
                        SessionMovieWithHall,
                        SessionMovieWithMovie,
                        SessionMovieWithDatetime,
                        SessionMovieBase,
                        SessionMovieDB,
                        ):
    pass


class MovieWithTicketsCount(BaseORM):
    Movie: MovieInSessionMovie = Field(exclude=True)
    movie: str = ""
    count_tickets: int

    @root_validator
    def get_movie_title(cls, values):
        values["movie"] = values["Movie"].title
        return values
