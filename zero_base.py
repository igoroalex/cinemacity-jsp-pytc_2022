from app.database import engine, Base, get_db
import app.models as models
import app.schemas as schemas


def add_movies():
    def add_movie(request_body):
        db = next(get_db())
        cur_model = schemas.MovieCreate(**request_body)

        cur_record = models.Movie(**cur_model.dict(exclude={"with_imdb"}))
        cur_record.save(db)
        if request_body["with_imdb"]:
            cur_record.save_from_imdb(db)

    add_movie({
        "imdb_id": "0133093",
        "title": "Matrix",
        "with_imdb": False,
    })
    add_movie({
        "imdb_id": "1375666",
        "title": "Inception",
        "with_imdb": False,
    })
    add_movie({
        "imdb_id": "789456",
        "title": "Hulk",
        "with_imdb": False,
    })
    add_movie({
        "imdb_id": "7894556",
        "title": "Hulk3",
        "with_imdb": False,
    })
    add_movie({
        "imdb_id": "7894567",
        "title": "Hulk3",
        "with_imdb": False,
    })


def add_genres():
    def add_genre(request_body):
        db = next(get_db())
        cur_model = schemas.Genre(**request_body)

        cur_record = models.Genre(**cur_model.dict())
        cur_record.save(db)

    add_genre({"name": "action"})
    add_genre({"name": "scy-fy"})
    add_genre({"name": "drama"})
    add_genre({"name": "comedy"})


def add_genres_movies():
    def add_genres_movie(request_body):
        db = next(get_db())
        cur_model = schemas.GenreMovie(**request_body)

        cur_record = models.GenreMovie(**cur_model.dict())
        cur_record.save(db)

    add_genres_movie({
        "movie_id": 1,
        "genre_id": 1,
    })
    add_genres_movie({
        "movie_id": 1,
        "genre_id": 2,
    })
    add_genres_movie({
        "movie_id": 2,
        "genre_id": 1,
    })


def add_persons():
    def add_person(request_body):
        db = next(get_db())
        cur_model = schemas.Person(**request_body)

        cur_record = models.Person(**cur_model.dict())
        cur_record.save(db)

    add_person({"name": "Keanu"})
    add_person({"name": "Ann"})
    add_person({"name": "Fishborn"})
    add_person({"name": "Wachovsky"})


def add_casts():
    def add_cast(request_body):
        db = next(get_db())
        cur_model = schemas.Cast(**request_body)

        cur_record = models.CastMovie(**cur_model.dict())
        cur_record.save(db)

    add_cast({
        "movie_id": 1,
        "person_id": 1,
    })
    add_cast({
        "movie_id": 1,
        "person_id": 2,
    })
    add_cast({
        "movie_id": 2,
        "person_id": 1,
    })


def add_directors():
    def add_director(request_body):
        db = next(get_db())
        cur_model = schemas.Director(**request_body)

        cur_record = models.DirectorMovie(**cur_model.dict())
        cur_record.save(db)

    add_director({
        "movie_id": 1,
        "person_id": 4,
    })
    add_director({
        "movie_id": 1,
        "person_id": 2,
    })
    add_director({
        "movie_id": 2,
        "person_id": 1,
    })


def add_halls():

    def add_hall(request_body):
        db = next(get_db())
        cur_model = schemas.HallCreate(**request_body)

        cur_record = models.Hall(**cur_model.dict())
        cur_record.save(db)

    add_hall({
        "name": "green",
        "number_of_seats": 21,
    })
    add_hall({
        "name": "blue",
        "number_of_seats": 10,
    })


def add_sessions_movies():

    def add_session_movie(request_body):
        db = next(get_db())
        cur_model = schemas.SessionMovieCreate(**request_body)

        cur_record = models.SessionMovie(**cur_model.dict())
        cur_record.save(db)

    add_session_movie({
        "date": "2022-05-18",
        "time": "21:50",
        "hall_id": 1,
        "movie_id": 1,
    })
    add_session_movie({
        "date": "2022-05-20",
        "time": "22:00",
        "hall_id": 2,
        "movie_id": 1,
    })
    add_session_movie({
        "date": "2022-05-30",
        "time": "22:50",
        "hall_id": 1,
        "movie_id": 2,
    })


def add_roles():

    def add_role(request_body):
        db = next(get_db())
        cur_model = schemas.RoleCreate(**request_body)

        cur_record = models.Role(**cur_model.dict())
        cur_record.save(db)

    add_role({
        "name": "admin",
    })
    add_role({
        "name": "viewer",
    })


def add_users():

    def add_user(request_body):
        db = next(get_db())
        cur_model = schemas.UserCreate(**request_body)

        cur_record = models.User(**cur_model.dict(exclude={"password"}))
        cur_record.hash_password(request_body["password"])
        cur_record.save(db)

    add_user({
        "email": "admin@gmail.com",
        "name": "aaa",
        "password": "1",
        "role_id": 1,
    })
    add_user({
        "email": "viewer@gmail.com",
        "name": "aaa",
        "password": "aaa",
        "role_id": 2,
    })


def add_tickets():

    def add_ticket(request_body):
        db = next(get_db())

        cur_model = schemas.TicketCreate(**request_body)

        cur_record = models.Ticket(**cur_model.dict())
        cur_record.save(db)

    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 1})
    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 11})
    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 12})
    add_ticket({"user_id": 1, "session_movie_id": 1, "seat": 13})
    add_ticket({"user_id": 1, "session_movie_id": 3, "seat": 15})
    add_ticket({"user_id": 1, "session_movie_id": 3, "seat": 13})


def zero_base():
    Base.metadata.drop_all(bind=engine)

    Base.metadata.create_all(bind=engine)

    add_movies()
    add_genres()
    add_persons()
    add_halls()
    add_sessions_movies()
    add_roles()
    add_users()
    add_tickets()

    # add_genres_movies()
    # add_casts()
    # add_directors()


if __name__ == '__main__':
    zero_base()
